
package modelo;

public class CuentaBancaria {
    private String NumCuenta;
    private String FechaAper;
    private String NomBanco;
    private Float PorcRendi;
    private Float Saldo;
    
    //Constructores
    //Vacio
    public CuentaBancaria(){
        this.NumCuenta = "";
        this.FechaAper = "";
        this.NomBanco = "";
        this.PorcRendi = 0.0f;
        this.Saldo = 0.0f;
    }
    //Argumentos
    public CuentaBancaria(String NumCuenta,String FechaAper,String NomBanco,Float PorcRendi,Float Saldo){
        this.NumCuenta = NumCuenta;
        this.FechaAper = FechaAper;
        this.NomBanco = NomBanco;
        this.PorcRendi = PorcRendi;
        this.Saldo = Saldo;
    }
    //Copia
    public CuentaBancaria(CuentaBancaria CB){
        this.NumCuenta = CB.NumCuenta;
        this.FechaAper = CB.FechaAper;
        this.NomBanco = CB.NomBanco;
        this.PorcRendi = CB.PorcRendi;
        this.Saldo = CB.Saldo;
    }
    //Metodos --Diagrama
    public void Depositar(float cantidad){
        this.Saldo += cantidad;
    }
    public boolean Retirar(float cantidad){
        if(cantidad <= this.Saldo){
            this.Saldo -= cantidad;
            return true;
        }
        return false;
    }
    public float CalculoRendmiento(){
        float CalculoRendimiento = (this.PorcRendi / 100) * this.Saldo / 365;
        return CalculoRendimiento;
    }
}
